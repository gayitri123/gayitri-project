import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor(private http:HttpClient,private route:Router) { }

  ngOnInit() {
  }
  changepassword(y)
  {
   this.http.put("/nav/changepassword",y).subscribe(res=>
  {
    if(res["message"]=="password changed")
    {
      alert(res["message"])
      this.route.navigate(["/nav/login"])
    }
    else{
      this.route.navigate(["/nav/otp"])
    }
  })
  }

}