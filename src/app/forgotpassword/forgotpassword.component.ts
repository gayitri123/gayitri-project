import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  constructor(private http:HttpClient,private route:Router) { }

  ngOnInit() {
  }
  password(x){
    this.http.post('nav/forgotpassword',x).subscribe(res=>{
      alert(res["message"]);
      if(res["message"]=="user found"){
        this.route.navigate(['nav/otp'])
      }
      else{
        this.route.navigate(['nav/forgotpassword'])
      }
    })
  }
}