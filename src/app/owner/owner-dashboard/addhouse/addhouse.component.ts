import { Component, OnInit } from '@angular/core';
import { RegistredDataService } from 'src/app/registred-data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addhouse',
  templateUrl: './addhouse.component.html',
  styleUrls: ['./addhouse.component.css']
})
export class AddhouseComponent implements OnInit {

  constructor(private http:HttpClient,private register:RegistredDataService,private router:Router) { }

  ngOnInit() {
  }
  addHouse(data)
  {
    data.ownername=this.register.currentUsername[0].name
    console.log(data)
    this.http.post('/ownerdashboard/addhouse/',data).subscribe((res)=>{
         //form validation condition
      if(res['message']=="null value not inserted")
      {
           alert("please fill the required field")
      }
      if(res['message'] === "unauthorised access")
      {
        alert(res['message'])
        console.log(res['message'])
         this.router.navigate(['nav/login'])
      }
      else if(res["message"]=="session expired"){
        alert("session expired.......! please relogin");
        this.router.navigate(['nav/login']);
      }
      alert(res["message"])
       

    })
  }
}
