import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {
  constructor(private route:Router,private http:HttpClient) { }

  ngOnInit() {
  }
  otp(x)
{
  this.http.post('nav/otp',x).subscribe(res=>
  {
    if(res["message"]=="verifiedOTP")
    {
      
      this.route.navigate(["/nav/changepassword"])
    }
    else
    { alert(res["message"])
      this.route.navigate(["/nav/otp/"])
    }
   
  })
}
}