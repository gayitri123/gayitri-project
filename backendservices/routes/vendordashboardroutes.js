const exp=require('express');
//importing database object
const initdb=require('../DBConfig').initdb
const getdb=require('../DBConfig').getdb
initdb();
const vendordashboardroutes=exp.Router();
//importing checkAuthorization middleware
const checkAuthrization=require('../middleware/checkAuthorization');
//put request handler for edit profile
vendordashboardroutes.put('/editprofile',checkAuthrization,(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
 dbo.collection("vendor").updateOne({name:{$eq:req.body.name}},{$set:{date:req.body.date,mail:req.body.mail,number:req.body.number,password:req.body.password,
    usertype:req.body.usertype,address:req.body.address}},(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                        res.json({"message": "updated successfully"})
                    }
        })

})
//get request from whomtolet from seeing all houses
vendordashboardroutes.get('/whomtolet',(req,res)=>{
    dbo=getdb();
    dbo.collection('houses collection').find({status:{$eq:true}}).toArray((err,dataArray)=>{
        if(err){
            console.log('error in saving data')
            console.log(err)
        }
        else{
                    res.json({"message":dataArray})
                }
    })
})

//vendor instersted house post request 
vendordashboardroutes.post('/whomtolet',(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
    if (req.body.length==0)
    {
        res.json({message:"no data get from client"})
    }
    else{
        dbo.collection("whomtolet").insertOne(req.body,(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                res.json({message:"request sent  successfully"})
            }
        })
    }
})
//vendor dopayment
vendordashboardroutes.post('/dopayment',(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
     //form validation condition
     if(req.body.ownername==""||req.body.address==""||req.body.amount==""||req.body.date==""||req.body.accountnumber==""||req.body.ifsccode==""){
        res.json({"message":"null value not inserted"});
    }
    if (req.body.length==0)
    {
        res.json({message:"no data get from client"})
    }
    else{
        dbo.collection("vendorpayments").insertOne(req.body,(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                res.json({message:"payment successfully"})
            }
        })
    }
})
//vendor payment history
//ownerdashboard viewprofile grt handler
vendordashboardroutes.get('/vendorpaymenthistory/:name',(req,res)=>{
    var dbo=getdb();
    dbo.collection('vendorpayments').find({vendorname:{$eq:req.params.name}}).toArray((err,dataArray)=>{
        if(err){
            console.log(err)
        }
        else
        {
            res.json({data:dataArray})
        }
    })
})
module.exports=vendordashboardroutes