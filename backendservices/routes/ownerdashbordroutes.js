const exp=require('express');
//importing database object
const initdb=require('../DBConfig').initdb
const getdb=require('../DBConfig').getdb
initdb();
//importing checkAuthorization middleware
const checkAuthrization=require('../middleware/checkAuthorization');
var ownerdashboardRoutes=exp.Router();
//ownerdashboardRoutes handler
ownerdashboardRoutes.post('/addhouse',checkAuthrization,(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
    //form validation condition
    if(req.body.housetype==""||req.body.hrent==""||req.body.address==""||req.body.aggrement==""){
        res.json({"message":"null value not inserted"});
    }
    if (req.body.length==0)
    {
        res.json({message:"no data get from client"})
    }
    else{
        req.body.status=true
        dbo.collection("houses collection").insertOne(req.body,(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                res.json({message:"registered successfully"})
                //console.log(success)
            }
        })
    }
})
//ownerdashboard adding payments 
ownerdashboardRoutes.post('/addpayments',checkAuthrization,(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
    if (req.body.length==0)
    {
        res.json({message:"no data get from client"})
    }
    else{
        req.body.status=true
        dbo.collection("payments collection").insertOne(req.body,(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                res.json({message:"registered successfully"})
            }
        })
    }
})
//ownerdashboard get method
ownerdashboardRoutes.get('/viewhouse/:name',checkAuthrization,(req,res)=>{
    //console.log(req.params)
    var dbo=getdb();
        dbo.collection("houses collection").find({$and:[{ownername:{$eq:req.params.name}},{status:{$eq:true}}]}).toArray((err,data)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                //console.log(data)
                res.json({message:data})
            }
        })
})
//getting clients who are interseted
//ownerdashboard get method
ownerdashboardRoutes.get('/viewclient/:name',checkAuthrization,(req,res)=>{
    //console.log(req.params)
    var dbo=getdb();
        dbo.collection("whomtolet").find({ownername:{$eq:req.params.name}}).toArray((err,data)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                //console.log(data)
                res.json({message:data})
            }
        })
})
// put method handler for update the owner 
ownerdashboardRoutes.put('/editprofile',checkAuthrization,(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
        dbo.collection("owner").updateOne({name:{$eq:req.body.name}},{$set:{date:req.body.date,mail:req.body.mail,number:req.body.number,password:req.body.password,usertype:req.body.usertype,address:req.body.address}},(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                        res.json({"message": "updated successfully"})
                    }
        })
})
//update the request from client
// put method handler for update the owner 
ownerdashboardRoutes.put('/viewclient',checkAuthrization,(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
        dbo.collection("houses collection").updateOne({address:{$eq:req.body.address}},{$set:{reqstatus:req.body.reqstatus,vendorname:req.body.vendorname}},(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                        res.json({"message": "request sent successfully"})
                    }
        })
})
//deleting the house
ownerdashboardRoutes.put('/viewhouse',checkAuthrization,(req,res)=>{
    console.log(req.body)
    var dbo=getdb();
    dbo.collection('houses collection').updateOne({address:{$eq:req.body.address}},{$set:{status:false}},(err,result)=>{
        if (err)
        {
            console.log(err)
        }
        else{
            dbo.collection('houses collection').find({$and:[{status:{$eq:true}},{ownername:{$eq:req.body.ownername}}]}).toArray((err,dataArray)=>{
                if (err)
                {
                    console.log(err)
                }
                else
                {
                    res.json({message:"successfully deleted",data:dataArray})
                }
            })
        }
    })

})
//ownerdashboard viewprofile get handler
ownerdashboardRoutes.get('/viewprofile/:name',(req,res)=>{
    var dbo=getdb();
    dbo.collection('owner').find({name:{$eq:req.params.name}}).toArray((err,dataArray)=>{
        if(err){
            console.log(err)
        }
        else
        {
            res.json({data:dataArray})
        }
    })
})
//owner payments history
ownerdashboardRoutes.get('/paymenthistory/:name',(req,res)=>{
    var dbo=getdb();
    dbo.collection('vendorpayments').find({ownername:{$eq:req.params.name}}).toArray((err,dataArray)=>{
        if(err){
            console.log(err)
        }
        else
        {
            res.json({data:dataArray})
        }
    })
})
//ownerdashboard get method for view payments
ownerdashboardRoutes.get('/viewpayments/:name',(req,res)=>{
    //console.log(req.params)
    var dbo=getdb();
        dbo.collection("payments collection").find({ownername:{$eq:req.params.name}}).toArray((err,data)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                //console.log(data)
                res.json({message:data})
            }
        })
})
module.exports=ownerdashboardRoutes